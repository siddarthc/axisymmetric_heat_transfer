function [lhs,rhs] = getRightEdgeStencil(D,L,M,T,B,h,k,gammaxHi,etaxHi,sxHi)

% D*u(i,j) -L*u(i-1,j) -M*u(i+1,j) -T*u(i,j+1) -B*u(i,j-1) = S
% lhs(1) = ind; lhs(2) = ind-1; lhs(3) = ind+1; lhs(4) = ind+jump; lhs(5) =
% ind-jump

lhs = zeros(1,5);

if (gammaxHi == 0) % dirichilet
    lhs(1) = D;
    lhs(2) = -L;
    lhs(3) = 0;
    lhs(4) = -T;
    lhs(5) = -B;
    rhs = M*sxHi;
else % neumann or robin
    lhs(1) = D +2*h*etaxHi*M/gammaxHi;
    lhs(2) = -L -M;
    lhs(3) = 0;
    lhs(4) = -T;
    lhs(5) = -B;
    rhs = 2*h*sxHi*M/gammaxHi;
end

