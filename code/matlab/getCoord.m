function [x,y] = getCoord(domLo,domHi,nx,ny)

x = zeros(nx,ny);
y = zeros(size(x));

dx = (domHi(1) - domLo(1))/(nx - 1);
dy = (domHi(2) - domLo(2))/(ny - 1);

for i = 1:nx
    x(i,:) = domLo(1) + (i-1)*dx;
end

for j = 1:ny
    y(:,j) = domLo(2) + (j-1)*dy;
end

