function f = getSource(rCell,z)


n = size(rCell);
nr = n(1); nz = n(2);
rFace = get_rFace(rCell);
f = zeros(nr,nz);

q1 = 1.712e5;
q2 =1.712e5;

for j = 1:nz
    for i = 1:nr
        if (rCell(i,j) <= 1.04)
            f(i,j) = q1;%*rCell(i,j);
        else
            f(i,j) = q2;%*rCell(i,j);
        end
    end
end


