function [lhs,rhs] = getInteriorStencil(D,L,M,T,B,h,k)

% D*u(i,j) -L*u(i-1,j) -M*u(i+1,j) -T*u(i,j+1) -B*u(i,j-1) = S
% lhs(1) = ind; lhs(2) = ind-1; lhs(3) = ind+1; lhs(4) = ind+jump; lhs(5) =
% ind-jump

lhs = zeros(1,5);

lhs(1) = D;
lhs(2) = -L;
lhs(3) = -M;
lhs(4) = -T;
lhs(5) = -B;

rhs = 0;