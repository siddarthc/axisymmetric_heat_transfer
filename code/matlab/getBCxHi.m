function [gamma,eta,s] = getBCxHi(y)

% BC: gamma*phi,n + eta*phi = s
%NOTE: phi,n is in the direction normal to the boundary

if (y <= 2)
    h = 10;
else
    h = 10;
end

k = 0.15;
Tinf = 300;

gamma = k; 
eta = h; % if gamma = 0, this value doesnt matter
         % because the value of ghost node is being fixed in the stencil

s = h*Tinf;