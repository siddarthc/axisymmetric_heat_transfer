function [lhs,rhs] = getTopRightCorStencil(D,L,M,T,B,h,k,gammayHi,etayHi,syHi,gammaxHi,etaxHi,sxHi)

% D*u(i,j) -L*u(i-1,j) -M*u(i+1,j) -T*u(i,j+1) -B*u(i,j-1) = S
% lhs(1) = ind; lhs(2) = ind-1; lhs(3) = ind+1; lhs(4) = ind+jump; lhs(5) =
% ind-jump

lhs = zeros(1,5);

if (gammayHi == 0) % dirichilet
    lhs(1) = D;
    lhs(2) = -L;
    lhs(3) = -M;
    lhs(4) = 0;
    lhs(5) = -B;
    rhs = T*syHi;
else % neumann or robin
    lhs(1) = D +2*k*etayHi*T/gammayHi;
    lhs(2) = -L;
    lhs(3) = -M;
    lhs(4) = 0;
    lhs(5) = -B -T;
    rhs = 2*k*syHi*T/gammayHi;
end

if (gammaxHi == 0) % dirichilet on left
    rhs = rhs + M*sxHi;
else % neumann or robin on left
    lhs(1) = lhs(1) + 2*h*etaxHi*M/gammaxHi;
    lhs(2) = lhs(2) - M;
    rhs = rhs + 2*h*sxHi*M/gammaxHi;
end

