function [h,k] = getSpacing(x,y)

n = size(x);

nx = n(1);
ny = n(2);

h = zeros(nx,ny); % size includes ghost cell spacing
k = zeros(nx,ny); % size includes ghost cell spacing

for j = 1:ny-1
    for i = 1:nx-1
        h(i,j) = x(i+1,j) - x(i,j);
        k(i,j) = y(i,j+1) - y(i,j);
    end
    h(nx,j) = h(nx-1,j);
    k(nx,j) = k(nx-1,j);
end

h(:,ny) = h(:,ny-1);
k(:,ny) = k(:,ny-1);