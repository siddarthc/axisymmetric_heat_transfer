function [gamma,eta,s] = getBCyHi(x)

% BC: gamma*phi,n + eta*phi = s
%NOTE: phi,n is in the direction normal to the boundary
%if t<=0
h = 10;
Tinf = 300;
k = 21.8;

gamma = k; 
eta = h; % if gamma = 0, this value doesnt matter
         % because the value of ghost node is being fixed in the stencil

s =h*Tinf;

%else
    h = 10;
Tinf = 300;
k = 0.15;

gamma = k; 
eta = h; % if gamma = 0, this value doesnt matter
         % because the value of ghost node is being fixed in the stencil

s =h*Tinf;
%end
