function alpha = getAlpha(rCell,z)

n = size(rCell);
nr = n(1);
nz = n(2);
alpha = zeros(nr,nz);
rFace = get_rFace(rCell);
c1 = 500; % conductivity
rho1 = 8000;  % density
c2 = 500;
rho2 = 8000;

alpha1 = 0*c1*rho1;
alpha2 = 0*c2*rho2;

for j = 1:nz
    for i = 1:nr
        if (rCell(i,j) <= 1.04)
            alpha(i,j) = alpha1;%*rCell(i,j);
        else
            alpha(i,j) = alpha2;%*rCell(i,j);
        end
    end
end

alpha
