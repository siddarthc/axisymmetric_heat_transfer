function RHS = getRHS(f, phiOld, dt, alpha, h, k)

RHS = f*dt + alpha.*phiOld;

n = size(RHS);

nx = n(1);
ny = n(2);

for j = 2:ny
    for i = 2:nx
        RHS(i,j) = RHS(i,j)*(h(i-1,j)+h(i,j))*(k(i,j-1)+k(i,j))*0.25;
    end
    RHS(1,j) = RHS(1,j)*(h(1,j))*(k(1,j-1)+k(1,j))*0.5;
end

for i = 2:nx
    RHS(i,1) = RHS(i,1)*(h(i-1,j)+h(i,j))*(k(i,1))*0.5;
end

RHS(1,1) = RHS(1,1)*(h(1,1))*(k(1,1));