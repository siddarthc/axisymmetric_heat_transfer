function [gamma,eta,s] = getBCxLo(y)

% BC: gamma*phi,n + eta*phi = s
%NOTE: phi,n is always derivative in the positive direction

h = 10000;
Tinf = 300;
k = 0.15;
gamma =-1; %since the normal vector is in -ive x direction
eta = 0; % if gamma = 0, this value doesnt matter
         % because the value of ghost node is being fixed in the stencil

s = 0;
