function [rFace] = get_rFace(rCell)

n = size(rCell);

nr = n(1);
nz = n(2);

 rFace = zeros(nr+1,nz+1);

for j=1:nz
    for i=2:nr
        rFace(i,j) = rCell(i,j) + 0.5*(rCell(i,j) - rCell(i-1,j));
    end
    rFace(1,j) = rCell(1,j) - 0.5*(rCell(2,j) - rCell(1,j));
    rFace(nr+1,j) = rCell(nr,j) + 0.5*(rCell(nr,j) - rCell(nr-1,j));
end

rFace(:,nz+1) = rFace(:,nz);
