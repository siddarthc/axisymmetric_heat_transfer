function phiNew = solveParabolic(x,y,startTime,dt,endTime,initialPhi,alpha,beta,f,plotMax,plotMin)

%solves parabolic eqn of the form: 
%        alpha(x,y)*phi,t - (beta(x+1/2,y+1/2)phi,i),i = f(x,y)

% user has to set the functions:
% getAlpha, getBeta, getSource, getInitialSoln, getBCxLo, getBCxHi,
% getBCyLo, getBCyHi

if (not(size(x) == size(y)))
    sprintf('Error : size(x) is not equal to size(y)');
    return
end

[h,k] = getSpacing(x,y);

phiNew = initialPhi;
phiOld = zeros(size(phiNew));

if (plotMax == 1 || plotMin == 1)
    counter = 0;
    nt = ceil((endTime - startTime)/dt);
    time = startTime+dt:dt:endTime;
end

if (plotMax == 1)
    maxPhi = zeros(1,nt);
end

if (plotMin == 1)
    minPhi = zeros(1,nt);
end

RHS = zeros(size(x));
[L,R] = getDiscreteMatrix(alpha,beta,RHS,x,y,h,k,dt);


for t = startTime + dt : dt : endTime
    fprintf('solving for time t = %f \n',t)
    phiOld = phiNew;
    RHS = getRHS(f, phiOld, dt, alpha, h, k);
    S = RHS + R;
    phiNew = getSoln(L,S); % solve L(phi) = S
    
    if (plotMax == 1 || plotMin == 1)
        counter = counter + 1;
    end
    
    if (plotMax == 1)
        maxPhi(counter) = max(max(phiNew));
    end
    
    if (plotMin == 1)
        minPhi(counter) = min(min(phiNew));
    end
end

if (plotMax == 1)
    figure;
    plot(time,maxPhi,'-or')
    xlabel('time')
    ylabel('maxPhi')
end

if (plotMin == 1)
    figure;
    plot(time,minPhi,'-or')
    xlabel('time')
    ylabel('minPhi')
end
    