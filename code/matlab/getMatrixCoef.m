function [D,L,M,T,B] = getMatrixCoef(alpha,beta,h,k,dt,rCell)

rFace = get_rFace(rCell);

n = size(alpha);
nx = n(1);
ny = n(2);

D = zeros(n);
L = zeros(n);
M = zeros(n);
T = zeros(n);
B = zeros(n);

beta = beta*dt;

for j = 2:ny
    for i = 2:nx
        L(i,j) = 0.5*(1-(1/(2*i)))*((k(i,j-1))*(beta(i,j)) + (k(i,j))*(beta(i,j+1)))/(h(i-1,j));
        M(i,j) = 0.5*(1+(1/(2*i)))*((k(i,j-1))*(beta(i+1,j)) + (k(i,j))*(beta(i+1,j+1)))/(h(i,j)); 
        T(i,j) = 0.5*((h(i-1,j))*(beta(i,j+1)) + (h(i,j))*(beta(i+1,j+1)))/(k(i,j));
        B(i,j) = 0.5*((h(i-1,j))*(beta(i,j)) + (h(i,j))*(beta(i+1,j)))/(k(i,j-1));
        D(i,j) = L(i,j) + M(i,j) + T(i,j) + B(i,j) + (alpha(i,j))*(h(i-1,j) + h(i,j))*(k(i,j-1) + k(i,j))*0.25;
    end
    L(1,j) = 0.5*(1-(1/(2*1)))*((k(1,j-1))*(beta(1,j)) + (k(1,j))*(beta(1,j+1)))/(h(1,j));
    M(1,j) = 0.5*(1-+(1/(2*1)))*((k(1,j-1))*(beta(2,j)) + (k(1,j))*(beta(2,j+1)))/(h(1,j));
    T(1,j) = 0.5*(1)*((h(1,j))*(beta(1,j+1)) + (h(1,j))*(beta(2,j+1)))/(k(1,j));
    B(1,j) = 0.5*(1)*((h(1,j))*(beta(1,j)) + (h(1,j))*(beta(2,j)))/(k(1,j-1));
    D(1,j) = L(1,j) + M(1,j) + T(1,j) + B(1,j) + (alpha(1,j))*(h(1,j) + h(1,j))*(k(1,j-1) + k(1,j))*0.25;
end

for i = 2:nx
    L(i,1) = 0.5*(1-(1/(2*i)))*((k(i,1))*(beta(i,1)) + (k(i,1))*(beta(i,2)))/(h(i-1,1));
    M(i,1) = 0.5*(1+(1/(2*i)))*((k(i,1))*(beta(i+1,1)) + (k(i,1))*(beta(i+1,2)))/(h(i,1));
    T(i,1) = 0.5*(1)*((h(i-1,1))*(beta(i,2)) + (h(i,1))*(beta(i+1,2)))/(k(i,1));
    B(i,1) = 0.5*(1)*((h(i-1,1))*(beta(i,1)) + (h(i,1))*(beta(i+1,1)))/(k(i,1));
    D(i,1) = L(i,1) + M(i,1) + T(i,1) + B(i,1) + (alpha(i,1))*(h(i-1,1) + h(i,1))*(k(i,1) + k(i,1))*0.25;
end

L(1,1) = 0.5*(1-(1/(2*1)))*((k(1,1))*(beta(1,1)) + (k(1,1))*(beta(1,2)))/(h(1,1));
M(1,1) = 0.5*(1+(1/(2*1)))*((k(1,1))*(beta(2,1)) + (k(1,1))*(beta(2,2)))/(h(1,1)); 
T(1,1) = 0.5*(1)*((h(1,1))*(beta(1,2)) + (h(1,1))*(beta(2,2)))/(k(1,1));
B(1,1) = 0.5*(1)*((h(1,1))*(beta(1,1)) + (h(1,1))*(beta(2,1)))/(k(1,1));
D(1,1) = L(1,1) + M(1,1) + T(1,1) + B(1,1) + (alpha(1,1))*(h(1,1) + h(1,1))*(k(1,1) + k(1,1))*0.25;
    
