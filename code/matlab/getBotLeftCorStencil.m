function [lhs,rhs] = getBotLeftCorStencil(D,L,M,T,B,h,k,gammayLo,etayLo,syLo,gammaxLo,etaxLo,sxLo)

% D*u(i,j) -L*u(i-1,j) -M*u(i+1,j) -T*u(i,j+1) -B*u(i,j-1) = S
% lhs(1) = ind; lhs(2) = ind-1; lhs(3) = ind+1; lhs(4) = ind+jump; lhs(5) =
% ind-jump

lhs = zeros(1,5);

if (gammayLo == 0) % dirichilet on bottom
    lhs(1) = D;
    lhs(2) = 0;
    lhs(3) = -M;
    lhs(4) = -T;
    lhs(5) = 0;
    rhs = B*syLo;
else
    lhs(1) = D -2*k*etayLo*B/gammayLo;
    lhs(2) = 0;
    lhs(3) = -M;
    lhs(4) = -T -B;
    lhs(5) = 0;
    rhs = -2*k*syLo*B/gammayLo;
end

if (gammaxLo == 0) % dirichilet on left
    rhs = rhs + L*sxLo;
else % neumann or robin on left
    lhs(1) = lhs(1) - 2*h*etaxLo*L/gammaxLo;
    lhs(3) = lhs(3) - L;
    rhs = rhs -2*h*sxLo*L/gammaxLo;
end
    
