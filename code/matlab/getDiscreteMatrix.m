function [mat,S] = getDiscreteMatrix(alpha,beta,RHS,x,y,h,k,dt)

[D,L,M,T,B] = getMatrixCoef(alpha,beta,h,k,dt,x);

n = size(alpha);
nx = n(1);
ny = n(2);

mat = sparse(nx*ny);

ind = 0;
jump = nx;

S = RHS;

for j = 1:ny
    for i = 1:nx
        ind = ind+1;
        if (j == 1) % bottom edge
            [gammayLo,etayLo,syLo] = getBCyLo(x(i,j));
            if (i == 1) % bottom left corner
                [gammaxLo,etaxLo,sxLo] = getBCxLo(y(i,j));
                [lhs,rhs] = getBotLeftCorStencil(D(i,j),L(i,j),M(i,j),T(i,j),B(i,j),h(i,j),k(i,j),gammayLo,etayLo,syLo,gammaxLo,etaxLo,sxLo);
                mat(ind,ind) = lhs(1);
                mat(ind,ind+1) = lhs(3);
                mat(ind,ind+jump) = lhs(4);
            elseif (i == nx) % bottom right corner
                [gammaxHi,etaxHi,sxHi] = getBCxHi(y(i,j));
                [lhs,rhs] = getBotRightCorStencil(D(i,j),L(i,j),M(i,j),T(i,j),B(i,j),h(i,j),k(i,j),gammayLo,etayLo,syLo,gammaxHi,etaxHi,sxHi);
                mat(ind,ind) = lhs(1);
                mat(ind,ind-1) = lhs(2);
                mat(ind,ind+jump) = lhs(4);
            else
                [lhs,rhs] = getBotEdgeStencil(D(i,j),L(i,j),M(i,j),T(i,j),B(i,j),h(i,j),k(i,j),gammayLo,etayLo,syLo);
                mat(ind,ind) = lhs(1);
                mat(ind,ind-1) = lhs(2);
                mat(ind,ind+1) = lhs(3);
                mat(ind,ind+jump) = lhs(4);
            end
        elseif (j == ny) % top edge
            [gammayHi,etayHi,syHi] = getBCyHi(x(i,j));
            if (i == 1) % top left corner
                [gammaxLo,etaxLo,sxLo] = getBCxLo(y(i,j));
                [lhs,rhs] = getTopLeftCorStencil(D(i,j),L(i,j),M(i,j),T(i,j),B(i,j),h(i,j),k(i,j),gammayHi,etayHi,syHi,gammaxLo,etaxLo,sxLo);
                mat(ind,ind) = lhs(1);
                mat(ind,ind+1) = lhs(3);
                mat(ind,ind-jump) = lhs(5);
            elseif (i == nx) % top right corner
                [gammaxHi,etaxHi,sxHi] = getBCxHi(y(i,j));
                [lhs,rhs] = getTopRightCorStencil(D(i,j),L(i,j),M(i,j),T(i,j),B(i,j),h(i,j),k(i,j),gammayHi,etayHi,syHi,gammaxHi,etaxHi,sxHi);
                mat(ind,ind) = lhs(1);
                mat(ind,ind-1) = lhs(2);
                mat(ind,ind-jump) = lhs(5);
            else
                [lhs,rhs] = getTopEdgeStencil(D(i,j),L(i,j),M(i,j),T(i,j),B(i,j),h(i,j),k(i,j),gammayHi,etayHi,syHi);
                mat(ind,ind) = lhs(1);
                mat(ind,ind-1) = lhs(2);
                mat(ind,ind+1) = lhs(3);
                mat(ind,ind-jump) = lhs(5);
            end
        elseif (i == 1) % left edge
            if (not(or(j == 1 , j == ny))) % corners already done
                [gammaxLo,etaxLo,sxLo] = getBCxLo(y(i,j));
                [lhs,rhs] = getLeftEdgeStencil(D(i,j),L(i,j),M(i,j),T(i,j),B(i,j),h(i,j),k(i,j),gammaxLo,etaxLo,sxLo);
                mat(ind,ind) = lhs(1);
                mat(ind,ind+1) = lhs(3);
                mat(ind,ind+jump) = lhs(4);
                mat(ind,ind-jump) = lhs(5);
            end
        elseif (i == nx) % right edge
            if (not(or(j == 1 , j == ny))) % corners already done
                [gammaxHi,etaxHi,sxHi] = getBCxHi(y(i,j));
                [lhs,rhs] = getRightEdgeStencil(D(i,j),L(i,j),M(i,j),T(i,j),B(i,j),h(i,j),k(i,j),gammaxHi,etaxHi,sxHi);
                mat(ind,ind) = lhs(1);
                mat(ind,ind-1) = lhs(2);
                mat(ind,ind+jump) = lhs(4);
                mat(ind,ind-jump) = lhs(5);
            end
        else
            [lhs,rhs] = getInteriorStencil(D(i,j),L(i,j),M(i,j),T(i,j),B(i,j),h(i,j),k(i,j));
            mat(ind,ind) = lhs(1);
            mat(ind,ind-1) = lhs(2);
            mat(ind,ind+1) = lhs(3);
            mat(ind,ind+jump) = lhs(4);
            mat(ind,ind-jump) = lhs(5);
        end
        S(i,j) = S(i,j) + rhs;
    end
end

