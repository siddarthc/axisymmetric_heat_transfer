function phiInit = getInitialSoln(x,y)

phiInit = ones(size(x));

c = 300;
phiInit = c*phiInit;