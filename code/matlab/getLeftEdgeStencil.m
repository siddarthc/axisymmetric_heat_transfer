function [lhs,rhs] = getLeftEdgeStencil(D,L,M,T,B,h,k,gammaxLo,etaxLo,sxLo)

% D*u(i,j) -L*u(i-1,j) -M*u(i+1,j) -T*u(i,j+1) -B*u(i,j-1) = S
% lhs(1) = ind; lhs(2) = ind-1; lhs(3) = ind+1; lhs(4) = ind+jump; lhs(5) =
% ind-jump

lhs = zeros(1,5);

if (gammaxLo == 0) % dirichilet
    lhs(1) = D;
    lhs(2) = 0;
    lhs(3) = -M;
    lhs(4) = -T;
    lhs(5) = -B;
    rhs = L*sxLo;
else % neumann or robin
    lhs(1) = D -2*h*etaxLo*L/gammaxLo;
    lhs(2) = 0;
    lhs(3) = -M - L;
    lhs(4) = -T;
    lhs(5) = -B;
    rhs = -2*h*sxLo*L/gammaxLo;
end

