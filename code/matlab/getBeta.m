function beta = getBeta(rCell,z)

n = size(rCell);
nr = n(1);
nz = n(2);
beta = zeros(nr+1,nz+1); % since its defined on faces
                        % beta(1,:), etc are like ghost faces: scary..
                        % x-o-x-o-x - beta is defined on x (so size is n+1)
                        % so set up beta well to avoid consequences

% rFace = get_rFace(rCell);

k1 = 0.15; % conductivity
k2 = 0.15;

for j = 1:nz
    for i = 1:nr
        if (rCell(i,j) <= 0.04)
            beta(i,j) = k1;
        else
            beta(i,j) = k2;
        end
    end
end

beta (nr+1,:) = k2;
beta(:,nz+1) = k2;
beta
% 
% beta = beta.*rFace;
