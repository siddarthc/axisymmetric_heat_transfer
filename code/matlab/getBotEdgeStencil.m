function [lhs,rhs] = getBotEdgeStencil(D,L,M,T,B,h,k,gammayLo,etayLo,syLo)

% D*u(i,j) -L*u(i-1,j) -M*u(i+1,j) -T*u(i,j+1) -B*u(i,j-1) = S
% lhs(1) = ind; lhs(2) = ind-1; lhs(3) = ind+1; lhs(4) = ind+jump; lhs(5) =
% ind-jump

lhs = zeros(1,5);

if (gammayLo == 0) % dirichilet
    lhs(1) = D;
    lhs(2) = -L;
    lhs(3) = -M;
    lhs(4) = -T;
    lhs(5) = 0;
    rhs = B*syLo;
else % neumann or robin
    lhs(1) = D -2*k*etayLo*B/gammayLo;
    lhs(2) = -L;
    lhs(3) = -M;
    lhs(4) = -T -B;
    lhs(5) = 0;
    rhs = -2*k*syLo*B/gammayLo;
end

