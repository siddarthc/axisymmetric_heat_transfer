function [lhs,rhs] = getBotRightCorStencil(D,L,M,T,B,h,k,gammayLo,etayLo,syLo,gammaxHi,etaxHi,sxHi)

% D*u(i,j) -L*u(i-1,j) -M*u(i+1,j) -T*u(i,j+1) -B*u(i,j-1) = S
% lhs(1) = ind; lhs(2) = ind-1; lhs(3) = ind+1; lhs(4) = ind+jump; lhs(5) =
% ind-jump

lhs = zeros(1,5);

if (gammayLo == 0) % dirichilet on bottom
    lhs(1) = D;
    lhs(2) = -L;
    lhs(3) = 0;
    lhs(4) = -T;
    lhs(5) = 0;
    rhs = B*syLo;
else
    lhs(1) = D -2*k*etayLo*B/gammayLo;
    lhs(2) = -L;
    lhs(3) = 0;
    lhs(4) = -T -B;
    lhs(5) = 0;
    rhs = -2*k*syLo*B/gammayLo;
end

if (gammaxHi == 0) % dirichilet on left
    rhs = rhs + M*sxHi;
else % neumann or robin on left
    lhs(1) = lhs(1) + 2*h*etaxHi*M/gammaxHi;
    lhs(2) = lhs(2) - M;
    rhs = rhs + 2*h*sxHi*M/gammaxHi;
end

