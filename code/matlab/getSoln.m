function phiNew = getSoln(L,S)

n = size(S);
nx = n(1);
ny = n(2);

RHS = zeros(1,nx*ny);

ind = 0;
for j = 1:ny
    for i = 1:nx
        ind = ind+1;
        RHS(ind) = S(i,j);
    end
end

RHS = RHS';

LHS = L\RHS;

ind = 0;
phiNew = zeros(nx,ny);
for j = 1:ny
    for i = 1:nx
        ind = ind+1;
        phiNew(i,j) = LHS(ind,1);
    end
end

