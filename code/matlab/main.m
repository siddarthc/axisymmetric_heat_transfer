% main
%tic
%close all
%clear all
domLo = [1 1];
domHi = [1.013 1.066];

nr = 101; nz = 301;
startTime = 0;
endTime = 10;
dt = 1;

[rCell,z] = getCoord(domLo,domHi,nr,nz);
initialPhi = getInitialSoln(rCell,z);
alpha = getAlpha(rCell,z);
beta = getBeta(rCell,z);
source = getSource(rCell,z);
rFace=get_rFace(rCell);

phiNew = solveParabolic(rCell,z,startTime,dt,endTime,initialPhi,alpha,beta,source,1,1);

%[x,y] = getCoord(domLo,domHi,nx,ny);

figure;
contourf(rCell,z,phiNew,10)

%toc
