import sys
#sys.path.append('/home/sid/libermate')

import numpy as np
import scipy
#import matcompat

# if available import pylab (from matlibplot)
try:
    import matplotlib.pylab as plt
except ImportError:
    pass

def getRightEdgeStencil(D, L, M, T, B, h, k, gammaxHi, etaxHi, sxHi):

    # Local Variables: B, D, h, rhs, sxHi, M, L, lhs, T, gammaxHi, k, etaxHi
    # Function calls: zeros, getRightEdgeStencil
    #% D*u(i,j) -L*u(i-1,j) -M*u(i+1,j) -T*u(i,j+1) -B*u(i,j-1) = S
    #% lhs(1) = ind; lhs(2) = ind-1; lhs(3) = ind+1; lhs(4) = ind+jump; lhs(5) =
    #% ind-jump
    lhs = np.zeros((5))
    if gammaxHi == 0.:
        #% dirichilet
        lhs[0] = D
        lhs[1] = -L
        lhs[2] = 0.
        lhs[3] = -T
        lhs[4] = -B
        rhs = np.dot(M, sxHi)
    else:
        #% neumann or robin
        lhs[0] = D+(np.dot(np.dot(2.*h, etaxHi), M)/gammaxHi)
        lhs[1] = -L-M
        lhs[2] = 0.
        lhs[3] = -T
        lhs[4] = -B
        rhs = np.dot(np.dot(2.*h, sxHi), M)/gammaxHi
        
    
    return [lhs, rhs]
