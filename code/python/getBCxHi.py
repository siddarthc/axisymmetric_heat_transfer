import sys
#sys.path.append('/home/sid/libermate')

import numpy as np
import scipy
#import matcompat

# if available import pylab (from matlibplot)
try:
    import matplotlib.pylab as plt
except ImportError:
    pass

def getBCxHi(y):

    # Local Variables: h, k, Tinf, s, eta, y, gamma
    # Function calls: getBCxHi
    #% BC: gamma*phi,n + eta*phi = s
    #%NOTE: phi,n is in the direction normal to the boundary
    if y<=2.:
        h = 10.
    else:
        h = 10.
        
    
    k = 0.15
    Tinf = 300.
    gamma = k
    eta = h
    #% if gamma = 0, this value doesnt matter
    #% because the value of ghost node is being fixed in the stencil
    s = np.dot(h, Tinf)
    return [gamma, eta, s]
