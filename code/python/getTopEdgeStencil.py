import sys
#sys.path.append('/home/sid/libermate')

import numpy as np
import scipy
#import matcompat

# if available import pylab (from matlibplot)
try:
    import matplotlib.pylab as plt
except ImportError:
    pass

def getTopEdgeStencil(D, L, M, T, B, h, k, gammayHi, etayHi, syHi):

    # Local Variables: B, D, h, rhs, M, L, gammayHi, lhs, T, etayHi, k, syHi
    # Function calls: getTopEdgeStencil, zeros
    #% D*u(i,j) -L*u(i-1,j) -M*u(i+1,j) -T*u(i,j+1) -B*u(i,j-1) = S
    #% lhs(1) = ind; lhs(2) = ind-1; lhs(3) = ind+1; lhs(4) = ind+jump; lhs(5) =
    #% ind-jump
    lhs = np.zeros((5))
    if gammayHi == 0.:
        #% dirichilet
        lhs[0] = D
        lhs[1] = -L
        lhs[2] = -M
        lhs[3] = 0.
        lhs[4] = -B
        rhs = np.dot(T, syHi)
    else:
        #% neumann or robin
        lhs[0] = D+(np.dot(np.dot(2.*k, etayHi), T)/gammayHi)
        lhs[1] = -L
        lhs[2] = -M
        lhs[3] = 0.
        lhs[4] = -B-T
        rhs = np.dot(np.dot(2.*k, syHi), T)/gammayHi
        
    
    return [lhs, rhs]
