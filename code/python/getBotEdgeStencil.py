import sys
#sys.path.append('/home/sid/libermate')

import numpy as np
import scipy
#import matcompat

# if available import pylab (from matlibplot)
try:
    import matplotlib.pylab as plt
except ImportError:
    pass

def getBotEdgeStencil(D, L, M, T, B, h, k, gammayLo, etayLo, syLo):

    # Local Variables: B, D, h, rhs, M, L, syLo, lhs, T, etayLo, gammayLo, k
    # Function calls: zeros, getBotEdgeStencil
    #% D*u(i,j) -L*u(i-1,j) -M*u(i+1,j) -T*u(i,j+1) -B*u(i,j-1) = S
    #% lhs(1) = ind; lhs(2) = ind-1; lhs(3) = ind+1; lhs(4) = ind+jump; lhs(5) =
    #% ind-jump
    lhs = np.zeros((5))

    if gammayLo == 0.:
        #% dirichilet
        lhs[0] = D
        lhs[1] = -L
        lhs[2] = -M
        lhs[3] = -T
        lhs[4] = 0.
        rhs = np.dot(B, syLo)
    else:
        #% neumann or robin
        lhs[0] = D-(np.dot(np.dot(2.*k, etayLo), B)/gammayLo)
        lhs[1] = -L
        lhs[2] = -M
        lhs[3] = -T-B
        lhs[4] = 0.
        rhs = np.dot(np.dot(np.dot(-2., k), syLo), B)/gammayLo
        

    return [lhs, rhs]
