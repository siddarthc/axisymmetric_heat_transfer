import sys
#sys.path.append('/home/sid/libermate')

import numpy as np
import scipy
#import matcompat

from get_rFace import get_rFace

# if available import pylab (from matlibplot)
try:
    import matplotlib.pylab as plt
except ImportError:
    pass

def getAlpha(rCell, z):

    # Local Variables: alpha2, alpha1, i, rho2, rho1, j, n, rFace, nz, c2, alpha, nr, z, rCell, c1
    # Function calls: getAlpha, get_rFace, zeros, size
#    n = matcompat.size(rCell)
    n = rCell.shape
    nr = n[0]
    nz = n[1]
    alpha = np.zeros((int(nr), int(nz)))
    rFace = get_rFace(rCell)
    c1 = 500.
    #% conductivity
    rho1 = 8000.
    #% density
    c2 = 500.
    rho2 = 8000.
    alpha1 = np.dot(0.*c1, rho1)
    alpha2 = np.dot(0.*c2, rho2)
    for j in np.arange(1., (nz)+1):
        for i in np.arange(1., (nr)+1):
            if rCell[int(i)-1,int(j)-1]<=1.04:
                alpha[int(i)-1,int(j)-1] = alpha1
                #%*rCell(i,j);
            else:
                alpha[int(i)-1,int(j)-1] = alpha2
                #%*rCell(i,j);
                      
    return [alpha]
