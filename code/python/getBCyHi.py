import sys
#sys.path.append('/home/sid/libermate')

import numpy as np
import scipy
#import matcompat

# if available import pylab (from matlibplot)
try:
    import matplotlib.pylab as plt
except ImportError:
    pass

def getBCyHi(x):

    # Local Variables: h, k, Tinf, s, eta, x, gamma
    # Function calls: getBCyHi
    #% BC: gamma*phi,n + eta*phi = s
    #%NOTE: phi,n is in the direction normal to the boundary
    #%if t<=0
    h = 10.
    Tinf = 300.
    k = 21.8
    gamma = k
    eta = h
    #% if gamma = 0, this value doesnt matter
    #% because the value of ghost node is being fixed in the stencil
    s = np.dot(h, Tinf)
    #%else
    h = 10.
    Tinf = 300.
    k = 0.15
    gamma = k
    eta = h
    #% if gamma = 0, this value doesnt matter
    #% because the value of ghost node is being fixed in the stencil
    s = np.dot(h, Tinf)

    return [gamma, eta, s]
