import sys
#sys.path.append('/home/sid/libermate')

import numpy as np
import scipy
#import matcompat

# if available import pylab (from matlibplot)
try:
    import matplotlib.pylab as plt
except ImportError:
    pass

def getRHS(f, phiOld, dt, alpha, h, k):

    # Local Variables: f, i, h, RHS, j, n, nx, ny, alpha, dt, phiOld, k
    # Function calls: getRHS, size
    RHS = np.dot(f, dt)+alpha*phiOld
    n = RHS.shape
    nx = n[0]
    ny = n[1]
    for j in np.arange(2., (ny)+1):
        for i in np.arange(2., (nx)+1):
            RHS[int(i)-1,int(j)-1] = np.dot(np.dot(np.dot(RHS[int(i)-1,int(j)-1], h[int((i-1.))-1,int(j)-1]+h[int(i)-1,int(j)-1]), k[int(i)-1,int((j-1.))-1]+k[int(i)-1,int(j)-1]), 0.25)
            
        RHS[0,int(j)-1] = np.dot(np.dot(np.dot(RHS[0,int(j)-1], h[0,int(j)-1]), k[0,int((j-1.))-1]+k[0,int(j)-1]), 0.5)
        
    for i in np.arange(2., (nx)+1):
        RHS[int(i)-1,0] = np.dot(np.dot(np.dot(RHS[int(i)-1,0], h[int((i-1.))-1,int(j)-1]+h[int(i)-1,int(j)-1]), k[int(i)-1,0]), 0.5)
        
    RHS[0,0] = np.dot(np.dot(RHS[0,0], h[0,0]), k[0,0])
    return [RHS]
