import sys
import os
#sys.path.append('/home/sid/libermate')

import numpy as np
import scipy
#import matcompat

from getSpacing import getSpacing
from getDiscreteMatrix import getDiscreteMatrix
from getRHS import getRHS
from getSoln import getSoln

# if available import pylab (from matlibplot)
try:
    import matplotlib.pylab as plt
except ImportError:
    pass

def solveParabolic(x, y, startTime, dt, endTime, initialPhi, alpha, beta, f, plotMax, plotMin):

    # Local Variables: k, maxPhi, phiNew, nt, plotMin, L, beta, R, startTime, alpha, dt, initialPhi, endTime, f, h, RHS, counter, S, t, time, y, x, phiOld, minPhi, plotMax
    # Function calls: plot, getRHS, figure, getSoln, max, min, getSpacing, ceil, not, zeros, sprintf, ylabel, solveParabolic, fprintf, getDiscreteMatrix, xlabel, size
    #%solves parabolic eqn of the form: 
    #%        alpha(x,y)*phi,t - (beta(x+1/2,y+1/2)phi,i),i = f(x,y)
    #% user has to set the functions:
    #% getAlpha, getBeta, getSource, getInitialSoln, getBCxLo, getBCxHi,
    #% getBCyLo, getBCyHi
    if (not((x.shape == y.shape))):
        sprintf('Error : size(x) is not equal to size(y)')
        return []
    
    
    [h, k] = getSpacing(x, y)
    phiNew = initialPhi
#    phiOld = np.zeros(matcompat.size(phiNew))
    phiOld = np.zeros(x.shape)
    if plotMax == 1. or plotMin == 1.:
        counter = 0.
        nt = np.ceil((endTime-startTime)/dt)
        time = np.arange(startTime+dt, (endTime)+(dt), dt)
    
    
    if plotMax == 1.:
        maxPhi = np.zeros((int(nt)))
    
    
    if plotMin == 1.:
        minPhi = np.zeros((int(nt)))
    
    
    RHS = np.zeros(x.shape)
    [L, R] = getDiscreteMatrix(alpha, beta, RHS, x, y, h, k, dt)
    for t in np.arange(startTime+dt, (endTime)+(dt), dt):
#        fprintf('solving for time t = %f \n', t)
        s = 'solving for time t = ' + repr(t)
        print s
        phiOld = phiNew
        [RHS] = getRHS(f, phiOld, dt, alpha, h, k)
        S = RHS+R
        [phiNew] = getSoln(L, S)
        #% solve L(phi) = S
        if plotMax == 1. or plotMin == 1.:
            counter = counter+1.
        
        
        if plotMax == 1.:
            maxPhi[int(counter)-1] = np.amax(phiNew)
            #print maxPhi[int(counter)-1]

        
        if plotMin == 1.:
            minPhi[int(counter)-1] = np.amin(phiNew)
            #print minPhi[int(counter)-1]
        
        
    if plotMax == 1.:
        fig = plt.figure()
        plt.plot(time, maxPhi, '-or')
        plt.xlabel('time')
        plt.ylabel('maxPhi')
        plt.show()
        if not os.path.isdir('Images'):
            os.mkdir('Images') 
        fig.savefig('./Images/maxTemp.png',format='png',dpi=1200)
    
    if plotMin == 1.:
        fig = plt.figure()
        plt.plot(time, minPhi, '-or')
        plt.xlabel('time')
        plt.ylabel('minPhi')
        plt.show()
        if not os.path.isdir('Images'):
            os.mkdir('Images')
        fig.savefig('./Images/minTemp.png',format='png',dpi=1200)
    
    return [phiNew]
