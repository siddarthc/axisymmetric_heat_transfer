import sys
#sys.path.append('/home/sid/libermate')

import numpy as np
import scipy
#import matcompat

# if available import pylab (from matlibplot)
try:
    import matplotlib.pylab as plt
except ImportError:
    pass

def getBeta(rCell, z):

    # Local Variables: i, j, beta, n, k2, nz, nr, z, rCell, k1
    # Function calls: zeros, getBeta, size
    n = rCell.shape
    nr = n[0]
    nz = n[1]
    beta = np.zeros((int(nr+1), int(nz+1)))
    #% since its defined on faces
    #% beta(1,:), etc are like ghost faces: scary..
    #% x-o-x-o-x - beta is defined on x (so size is n+1)
    #% so set up beta well to avoid consequences
    #% rFace = get_rFace(rCell);
    k1 = 0.15
    #% conductivity
    k2 = 0.15
    for j in np.arange(1, (nz)+1):
        for i in np.arange(1, (nr)+1):
            if rCell[int(i)-1,int(j)-1]<=0.04:
                beta[int(i)-1,int(j)-1] = k1
            else:
                beta[int(i)-1,int(j)-1] = k2
                
                    
    beta[int((nr+1))-1,:] = k2
    beta[:,int((nz+1))-1] = k2
    #beta
    #% 
    #% beta = beta.*rFace;
    return [beta]
