import sys
#sys.path.append('/home/sid/libermate')

import numpy as np
import scipy
#import matcompat

# if available import pylab (from matlibplot)
try:
    import matplotlib.pylab as plt
except ImportError:
    pass

def getCoord(domLo, domHi, nx, ny):

    # Local Variables: domHi, i, domLo, j, nx, ny, dx, dy, y, x
    # Function calls: size, zeros, getCoord
    x = np.zeros((int(nx), int(ny)))
    y = np.zeros((int(nx), int(ny)))
    dx = (domHi[0]-domLo[0])/(nx-1.)
    dy = (domHi[1]-domLo[1])/(ny-1.)
    for i in np.arange(1., (nx)+1):
        x[int(i)-1,:] = domLo[0]+np.dot(i-1., dx)
        
    for j in np.arange(1., (ny)+1):
        y[:,int(j)-1] = domLo[1]+np.dot(j-1., dy)
        
    return [x, y]
