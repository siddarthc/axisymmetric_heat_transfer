import sys
#sys.path.append('/home/sid/libermate')

import numpy as np
import scipy
#import matcompat

# if available import pylab (from matlibplot)
try:
    import matplotlib.pylab as plt
except ImportError:
    pass

def get_rFace(rCell):

    # Local Variables: i, j, n, rFace, nz, nr, rCell
    # Function calls: get_rFace, zeros, size
    n = rCell.shape
    nr = n[0]
    nz = n[1]
    rFace = np.zeros((int(nr+1.), int(nz+1.)))
    for j in np.arange(1., (nz)+1):
        for i in np.arange(2., (nr)+1):
            rFace[int(i)-1,int(j)-1] = rCell[int(i)-1,int(j)-1]+np.dot(0.5, rCell[int(i)-1,int(j)-1]-rCell[int((i-1.))-1,int(j)-1])
            
        rFace[0,int(j)-1] = rCell[0,int(j)-1]-np.dot(0.5, rCell[1,int(j)-1]-rCell[0,int(j)-1])
        rFace[int((nr+1.))-1,int(j)-1] = rCell[int(nr)-1,int(j)-1]+np.dot(0.5, rCell[int(nr)-1,int(j)-1]-rCell[int((nr-1.))-1,int(j)-1])
        
    rFace[:,int((nz+1.))-1] = rFace[:,int(nz)-1]
    return [rFace]
