import sys
#sys.path.append('/home/sid/libermate')

import numpy as np
import scipy
#import matcompat

# if available import pylab (from matlibplot)
try:
    import matplotlib.pylab as plt
except ImportError:
    pass

def getBCyLo(x):

    # Local Variables: h, k, Tinf, s, eta, x, gamma
    # Function calls: getBCyLo
    #% BC: gamma*phi,n + eta*phi = s
    #%NOTE: phi,n is in the direction normal to the boundary
    h = 10.
    Tinf = 300.
    k = 0.15
    gamma = -k
    #%-k; % since unit normal is in the -ive y direction
    eta = h
    #% if gamma = 0, this value doesnt matter
    #% because the value of ghost node is being fixed in the stencil
    s = np.dot(h, Tinf)
    return [gamma, eta, s]
