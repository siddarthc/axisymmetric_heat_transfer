import sys
#sys.path.append('/home/sid/libermate')

import numpy as np
import scipy
#import matcompat

from get_rFace import get_rFace

# if available import pylab (from matlibplot)
try:
    import matplotlib.pylab as plt
except ImportError:
    pass

def getMatrixCoef(alpha, beta, h, k, dt, rCell):

    # Local Variables: nx, B, D, i, h, k, j, M, L, rCell, rFace, ny, beta, T, alpha, dt, n
    # Function calls: get_rFace, zeros, getMatrixCoef, size
    [rFace] = get_rFace(rCell)
    n = alpha.shape
    nx = n[0]
    ny = n[1]
    D = np.zeros(n)
    L = np.zeros(n)
    M = np.zeros(n)
    T = np.zeros(n)
    B = np.zeros(n)
    beta = np.dot(beta, dt)
    for j in np.arange(2., (ny)+1):
        for i in np.arange(2., (nx)+1):
            L[int(i)-1,int(j)-1] = np.dot(np.dot(0.5, 1.-1./(2.*i)), np.dot(k[int(i)-1,int((j-1.))-1], beta[int(i)-1,int(j)-1])+np.dot(k[int(i)-1,int(j)-1], beta[int(i)-1,int((j+1.))-1]))/h[int((i-1.))-1,int(j)-1]
            M[int(i)-1,int(j)-1] = np.dot(np.dot(0.5, 1.+1./(2.*i)), np.dot(k[int(i)-1,int((j-1.))-1], beta[int((i+1.))-1,int(j)-1])+np.dot(k[int(i)-1,int(j)-1], beta[int((i+1.))-1,int((j+1.))-1]))/h[int(i)-1,int(j)-1]
            T[int(i)-1,int(j)-1] = np.dot(0.5, np.dot(h[int((i-1.))-1,int(j)-1], beta[int(i)-1,int((j+1.))-1])+np.dot(h[int(i)-1,int(j)-1], beta[int((i+1.))-1,int((j+1.))-1]))/k[int(i)-1,int(j)-1]
            B[int(i)-1,int(j)-1] = np.dot(0.5, np.dot(h[int((i-1.))-1,int(j)-1], beta[int(i)-1,int(j)-1])+np.dot(h[int(i)-1,int(j)-1], beta[int((i+1.))-1,int(j)-1]))/k[int(i)-1,int((j-1.))-1]
            D[int(i)-1,int(j)-1] = L[int(i)-1,int(j)-1]+M[int(i)-1,int(j)-1]+T[int(i)-1,int(j)-1]+B[int(i)-1,int(j)-1]+np.dot(np.dot(np.dot(alpha[int(i)-1,int(j)-1], h[int((i-1.))-1,int(j)-1]+h[int(i)-1,int(j)-1]), k[int(i)-1,int((j-1.))-1]+k[int(i)-1,int(j)-1]), 0.25)
            
        L[0,int(j)-1] = np.dot(np.dot(0.5, 1.-1./(2.*1.)), np.dot(k[0,int((j-1.))-1], beta[0,int(j)-1])+np.dot(k[0,int(j)-1], beta[0,int((j+1.))-1]))/h[0,int(j)-1]
        M[0,int(j)-1] = np.dot(np.dot(0.5, 1.+1./(2.*1.)), np.dot(k[0,int((j-1.))-1], beta[1,int(j)-1])+np.dot(k[0,int(j)-1], beta[1,int((j+1.))-1]))/h[0,int(j)-1]
        T[0,int(j)-1] = np.dot(0.5*1., np.dot(h[0,int(j)-1], beta[0,int((j+1.))-1])+np.dot(h[0,int(j)-1], beta[1,int((j+1.))-1]))/k[0,int(j)-1]
        B[0,int(j)-1] = np.dot(0.5*1., np.dot(h[0,int(j)-1], beta[0,int(j)-1])+np.dot(h[0,int(j)-1], beta[1,int(j)-1]))/k[0,int((j-1.))-1]
        D[0,int(j)-1] = L[0,int(j)-1]+M[0,int(j)-1]+T[0,int(j)-1]+B[0,int(j)-1]+np.dot(np.dot(np.dot(alpha[0,int(j)-1], h[0,int(j)-1]+h[0,int(j)-1]), k[0,int((j-1.))-1]+k[0,int(j)-1]), 0.25)
        
    for i in np.arange(2., (nx)+1):
        L[int(i)-1,0] = np.dot(np.dot(0.5, 1.-1./(2.*i)), np.dot(k[int(i)-1,0], beta[int(i)-1,0])+np.dot(k[int(i)-1,0], beta[int(i)-1,1]))/h[int((i-1.))-1,0]
        M[int(i)-1,0] = np.dot(np.dot(0.5, 1.+1./(2.*i)), np.dot(k[int(i)-1,0], beta[int((i+1.))-1,0])+np.dot(k[int(i)-1,0], beta[int((i+1.))-1,1]))/h[int(i)-1,0]
        T[int(i)-1,0] = np.dot(0.5*1., np.dot(h[int((i-1.))-1,0], beta[int(i)-1,1])+np.dot(h[int(i)-1,0], beta[int((i+1.))-1,1]))/k[int(i)-1,0]
        B[int(i)-1,0] = np.dot(0.5*1., np.dot(h[int((i-1.))-1,0], beta[int(i)-1,0])+np.dot(h[int(i)-1,0], beta[int((i+1.))-1,0]))/k[int(i)-1,0]
        D[int(i)-1,0] = L[int(i)-1,0]+M[int(i)-1,0]+T[int(i)-1,0]+B[int(i)-1,0]+np.dot(np.dot(np.dot(alpha[int(i)-1,0], h[int((i-1.))-1,0]+h[int(i)-1,0]), k[int(i)-1,0]+k[int(i)-1,0]), 0.25)
        
    L[0,0] = np.dot(np.dot(0.5, 1.-1./(2.*1.)), np.dot(k[0,0], beta[0,0])+np.dot(k[0,0], beta[0,1]))/h[0,0]
    M[0,0] = np.dot(np.dot(0.5, 1.+1./(2.*1.)), np.dot(k[0,0], beta[1,0])+np.dot(k[0,0], beta[1,1]))/h[0,0]
    T[0,0] = np.dot(0.5*1., np.dot(h[0,0], beta[0,1])+np.dot(h[0,0], beta[1,1]))/k[0,0]
    B[0,0] = np.dot(0.5*1., np.dot(h[0,0], beta[0,0])+np.dot(h[0,0], beta[1,0]))/k[0,0]
    D[0,0] = L[0,0]+M[0,0]+T[0,0]+B[0,0]+np.dot(np.dot(np.dot(alpha[0,0], h[0,0]+h[0,0]), k[0,0]+k[0,0]), 0.25)

    return [D, L, M, T, B]
