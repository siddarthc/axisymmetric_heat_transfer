import sys
#sys.path.append('/home/sid/libermate')

import numpy as np
import scipy
#import matcompat

# if available import pylab (from matlibplot)
try:
    import matplotlib.pylab as plt
except ImportError:
    pass

def getInteriorStencil(D, L, M, T, B, h, k):

    # Local Variables: B, D, h, rhs, M, L, lhs, T, k
    # Function calls: getInteriorStencil, zeros
    #% D*u(i,j) -L*u(i-1,j) -M*u(i+1,j) -T*u(i,j+1) -B*u(i,j-1) = S
    #% lhs(1) = ind; lhs(2) = ind-1; lhs(3) = ind+1; lhs(4) = ind+jump; lhs(5) =
    #% ind-jump
    lhs = np.zeros((5))
    lhs[0] = D
    lhs[1] = -L
    lhs[2] = -M
    lhs[3] = -T
    lhs[4] = -B
    rhs = 0.
    return [lhs, rhs]
