import sys
#sys.path.append('/home/sid/libermate')

import numpy as np
import scipy
import scipy.sparse
#import matcompat

from getMatrixCoef import getMatrixCoef
from getBCxLo import getBCxLo
from getBCxHi import getBCxHi
from getBCyLo import getBCyLo
from getBCyHi import getBCyHi
from getBotLeftCorStencil import getBotLeftCorStencil
from getBotRightCorStencil import getBotRightCorStencil
from getTopLeftCorStencil import getTopLeftCorStencil
from getTopRightCorStencil import getTopRightCorStencil
from getBotEdgeStencil import getBotEdgeStencil
from getTopEdgeStencil import getTopEdgeStencil
from getRightEdgeStencil import getRightEdgeStencil
from getLeftEdgeStencil import getLeftEdgeStencil
from getInteriorStencil import getInteriorStencil

# if available import pylab (from matlibplot)
try:
    import matplotlib.pylab as plt
except ImportError:
    pass

def getDiscreteMatrix(alpha, beta, RHS, x, y, h, k, dt):

    # Local Variables: jump, syHi, ind, D, gammaxLo, sxLo, sxHi, syLo, nx, ny, B, mat, etayLo, rhs, M, L, gammayHi, S, T, gammaxHi, alpha, dt, gammayLo, i, h, RHS, j, beta, n, lhs, etaxLo, etayHi, y, x, k, etaxHi
    # Function calls: or, getBCxHi, getTopEdgeStencil, getRightEdgeStencil, getBCxLo, getTopLeftCorStencil, getBCyHi, getBotEdgeStencil, getInteriorStencil, getBotRightCorStencil, getDiscreteMatrix, getBotLeftCorStencil, sparse, not, getBCyLo, getLeftEdgeStencil, getTopRightCorStencil, getMatrixCoef, size
    [D, L, M, T, B] = getMatrixCoef(alpha, beta, h, k, dt, x)
    n = alpha.shape
    nx = n[0]
    ny = n[1]
    mat = np.zeros((nx*ny,nx*ny))
#    mat = scipy.sparse.coo_matrix(nx,ny)
    ind = 0.
    jump = nx
    S = RHS
    for j in np.arange(1., (ny)+1):
        for i in np.arange(1., (nx)+1):
            ind = ind+1.
            if j == 1.:
                #% bottom edge
                [gammayLo, etayLo, syLo] = getBCyLo(x[int(i)-1,int(j)-1])
                if i == 1.:
                    #% bottom left corner
                    [gammaxLo, etaxLo, sxLo] = getBCxLo(y[int(i)-1,int(j)-1])
                    [lhs, rhs] = getBotLeftCorStencil(D[int(i)-1,int(j)-1], L[int(i)-1,int(j)-1], M[int(i)-1,int(j)-1], T[int(i)-1,int(j)-1], B[int(i)-1,int(j)-1], h[int(i)-1,int(j)-1], k[int(i)-1,int(j)-1], gammayLo, etayLo, syLo, gammaxLo, etaxLo, sxLo)
                    mat[int(ind)-1,int(ind)-1] = lhs[0]
                    mat[int(ind)-1,int((ind+1.))-1] = lhs[2]
                    mat[int(ind)-1,int((ind+jump))-1] = lhs[3]
                elif i == nx:
                    #% bottom right corner
                    [gammaxHi, etaxHi, sxHi] = getBCxHi(y[int(i)-1,int(j)-1])
                    [lhs, rhs] = getBotRightCorStencil(D[int(i)-1,int(j)-1], L[int(i)-1,int(j)-1], M[int(i)-1,int(j)-1], T[int(i)-1,int(j)-1], B[int(i)-1,int(j)-1], h[int(i)-1,int(j)-1], k[int(i)-1,int(j)-1], gammayLo, etayLo, syLo, gammaxHi, etaxHi, sxHi)
                    mat[int(ind)-1,int(ind)-1] = lhs[0]
                    mat[int(ind)-1,int((ind-1.))-1] = lhs[1]
                    mat[int(ind)-1,int((ind+jump))-1] = lhs[3]
                else:
                    [lhs, rhs] = getBotEdgeStencil(D[int(i)-1,int(j)-1], L[int(i)-1,int(j)-1], M[int(i)-1,int(j)-1], T[int(i)-1,int(j)-1], B[int(i)-1,int(j)-1], h[int(i)-1,int(j)-1], k[int(i)-1,int(j)-1], gammayLo, etayLo, syLo)
                    mat[int(ind)-1,int(ind)-1] = lhs[0]
                    mat[int(ind)-1,int((ind-1.))-1] = lhs[1]
                    mat[int(ind)-1,int((ind+1.))-1] = lhs[2]
                    mat[int(ind)-1,int((ind+jump))-1] = lhs[3]
            elif j == ny:
                #% top edge
                [gammayHi, etayHi, syHi] = getBCyHi(x[int(i)-1,int(j)-1])
                if i == 1.:
                    #% top left corner
                    [gammaxLo, etaxLo, sxLo] = getBCxLo(y[int(i)-1,int(j)-1])
                    [lhs, rhs] = getTopLeftCorStencil(D[int(i)-1,int(j)-1], L[int(i)-1,int(j)-1], M[int(i)-1,int(j)-1], T[int(i)-1,int(j)-1], B[int(i)-1,int(j)-1], h[int(i)-1,int(j)-1], k[int(i)-1,int(j)-1], gammayHi, etayHi, syHi, gammaxLo, etaxLo, sxLo)
                    mat[int(ind)-1,int(ind)-1] = lhs[0]
                    mat[int(ind)-1,int((ind+1.))-1] = lhs[2]
                    mat[int(ind)-1,int((ind-jump))-1] = lhs[4]
                elif i == nx:
                    #% top right corner
                    [gammaxHi, etaxHi, sxHi] = getBCxHi(y[int(i)-1,int(j)-1])
                    [lhs, rhs] = getTopRightCorStencil(D[int(i)-1,int(j)-1], L[int(i)-1,int(j)-1], M[int(i)-1,int(j)-1], T[int(i)-1,int(j)-1], B[int(i)-1,int(j)-1], h[int(i)-1,int(j)-1], k[int(i)-1,int(j)-1], gammayHi, etayHi, syHi, gammaxHi, etaxHi, sxHi)
                    mat[int(ind)-1,int(ind)-1] = lhs[0]
                    mat[int(ind)-1,int((ind-1.))-1] = lhs[1]
                    mat[int(ind)-1,int((ind-jump))-1] = lhs[4]
                else:
                    [lhs, rhs] = getTopEdgeStencil(D[int(i)-1,int(j)-1], L[int(i)-1,int(j)-1], M[int(i)-1,int(j)-1], T[int(i)-1,int(j)-1], B[int(i)-1,int(j)-1], h[int(i)-1,int(j)-1], k[int(i)-1,int(j)-1], gammayHi, etayHi, syHi)
                    mat[int(ind)-1,int(ind)-1] = lhs[0]
                    mat[int(ind)-1,int((ind-1.))-1] = lhs[1]
                    mat[int(ind)-1,int((ind+1.))-1] = lhs[2]
                    mat[int(ind)-1,int((ind-jump))-1] = lhs[4]     
            elif i == 1.:
                #% left edge
                if (not((j == 1.) or (j == ny))):
                    #% corners already done
                    [gammaxLo, etaxLo, sxLo] = getBCxLo(y[int(i)-1,int(j)-1])
                    [lhs, rhs] = getLeftEdgeStencil(D[int(i)-1,int(j)-1], L[int(i)-1,int(j)-1], M[int(i)-1,int(j)-1], T[int(i)-1,int(j)-1], B[int(i)-1,int(j)-1], h[int(i)-1,int(j)-1], k[int(i)-1,int(j)-1], gammaxLo, etaxLo, sxLo)
                    mat[int(ind)-1,int(ind)-1] = lhs[0]
                    mat[int(ind)-1,int((ind+1.))-1] = lhs[2]
                    mat[int(ind)-1,int((ind+jump))-1] = lhs[3]
                    mat[int(ind)-1,int((ind-jump))-1] = lhs[4]
            elif i == nx:
                #% right edge
                if (not((j == 1.) or (j == ny))):
                    #% corners already done
                    [gammaxHi, etaxHi, sxHi] = getBCxHi(y[int(i)-1,int(j)-1])
                    [lhs, rhs] = getRightEdgeStencil(D[int(i)-1,int(j)-1], L[int(i)-1,int(j)-1], M[int(i)-1,int(j)-1], T[int(i)-1,int(j)-1], B[int(i)-1,int(j)-1], h[int(i)-1,int(j)-1], k[int(i)-1,int(j)-1], gammaxHi, etaxHi, sxHi)
                    mat[int(ind)-1,int(ind)-1] = lhs[0]
                    mat[int(ind)-1,int((ind-1.))-1] = lhs[1]
                    mat[int(ind)-1,int((ind+jump))-1] = lhs[3]
                    mat[int(ind)-1,int((ind-jump))-1] = lhs[4]
            else:
                [lhs, rhs] = getInteriorStencil(D[int(i)-1,int(j)-1], L[int(i)-1,int(j)-1], M[int(i)-1,int(j)-1], T[int(i)-1,int(j)-1], B[int(i)-1,int(j)-1], h[int(i)-1,int(j)-1], k[int(i)-1,int(j)-1])
                mat[int(ind)-1,int(ind)-1] = lhs[0]
                mat[int(ind)-1,int((ind-1.))-1] = lhs[1]
                mat[int(ind)-1,int((ind+1.))-1] = lhs[2]
                mat[int(ind)-1,int((ind+jump))-1] = lhs[3]
                mat[int(ind)-1,int((ind-jump))-1] = lhs[4]
                
            S[int(i)-1,int(j)-1] = S[int(i)-1,int(j)-1]+rhs
            
#    spMat = scipy.sparse.coo_matrix(mat)    
    return [mat, S]
