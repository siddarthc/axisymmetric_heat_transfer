import sys
#sys.path.append('/home/sid/libermate')

import numpy as np
import scipy
#import matcompat

# if available import pylab (from matlibplot)
try:
    import matplotlib.pylab as plt
except ImportError:
    pass

def getBCxLo(y):

    # Local Variables: h, k, Tinf, s, eta, y, gamma
    # Function calls: getBCxLo
    #% BC: gamma*phi,n + eta*phi = s
    #%NOTE: phi,n is always derivative in the positive direction
    h = 10000.
    Tinf = 300.
    k = 0.15
    gamma = -1.
    #%since the normal vector is in -ive x direction
    eta = 0.
    #% if gamma = 0, this value doesnt matter
    #% because the value of ghost node is being fixed in the stencil
    s = 0.

    return [gamma, eta, s]
