import sys
#sys.path.append('/home/sid/libermate')

import numpy as np
import scipy
#import matcompat

# if available import pylab (from matlibplot)
try:
    import matplotlib.pylab as plt
except ImportError:
    pass

def getInitialSoln(x, y):

    # Local Variables: phiInit, x, c, y
    # Function calls: getInitialSoln, ones, size
    tmp = x.shape
#    print tmp
#    phiInit = np.ones(matcompat.size(x))
    nx = tmp[0]
    ny = tmp[1]
    phiInit = np.zeros((nx,ny))
    c = 300.
#    phiInit = np.dot(c, phiInit)

    for i in np.arange(1., (nx)+1):
        for j in np.arange(1., (ny)+1):
            phiInit[int(i)-1, int(j)-1] = c

    return [phiInit]
