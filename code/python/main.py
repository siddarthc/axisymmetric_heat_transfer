import sys
import os
#sys.path.append('/home/sid/libermate')

import numpy as np
import scipy
#import matcompat

from getCoord import getCoord
from getInitialSoln import getInitialSoln
from getAlpha import getAlpha
from getBeta import getBeta
from getSource import getSource
from get_rFace import get_rFace
from solveParabolic import solveParabolic

# if available import pylab (from matlibplot)
try:
    import matplotlib.pylab as plt
except ImportError:
    pass

#% main
#%tic
#%close all
#%clear all
domLo = np.array(np.hstack((1., 1.)))
domHi = np.array(np.hstack((1.013, 1.066)))
nr = 21
nz = 61
startTime = 0.
endTime = 1.
dt = 1.
[rCell, z] = getCoord(domLo, domHi, nr, nz)
[initialPhi] = getInitialSoln(rCell, z)
[alpha] = getAlpha(rCell, z)
[beta] = getBeta(rCell, z)
[source] = getSource(rCell, z)
[rFace] = get_rFace(rCell)

[phiNew] = solveParabolic(rCell, z, startTime, dt, endTime, initialPhi, alpha, beta, source, 1., 1.)
#%[x,y] = getCoord(domLo,domHi,nx,ny);
fig = plt.figure()
cp = plt.contourf(rCell, z, phiNew, 10)
plt.colorbar(cp)
if not os.path.isdir('Images'):
            os.mkdir('Images')

fig.savefig('./Images/contour.png',format='png',dpi=1200)
plt.show()
#%toc
