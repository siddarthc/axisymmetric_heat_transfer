import sys
#sys.path.append('/home/sid/libermate')

import numpy as np
import scipy
#import matcompat

# if available import pylab (from matlibplot)
try:
    import matplotlib.pylab as plt
except ImportError:
    pass

def getSpacing(x, y):

    # Local Variables: i, h, k, j, n, nx, ny, y, x
    # Function calls: zeros, getSpacing, size
    n = x.shape
    nx = n[0]
    ny = n[1]
    h = np.zeros((int(nx), int(ny)))
    #% size includes ghost cell spacing
    k = np.zeros((int(nx), int(ny)))
    #% size includes ghost cell spacing
    for j in np.arange(1., (ny-1.)+1):
        for i in np.arange(1., (nx-1.)+1):
            h[int(i)-1,int(j)-1] = x[int((i+1.))-1,int(j)-1]-x[int(i)-1,int(j)-1]
            k[int(i)-1,int(j)-1] = y[int(i)-1,int((j+1.))-1]-y[int(i)-1,int(j)-1]
            
        h[int(nx)-1,int(j)-1] = h[int((nx-1.))-1,int(j)-1]
        k[int(nx)-1,int(j)-1] = k[int((nx-1.))-1,int(j)-1]
        
    h[:,int(ny)-1] = h[:,int((ny-1.))-1]
    k[:,int(ny)-1] = k[:,int((ny-1.))-1]
    return [h, k]
