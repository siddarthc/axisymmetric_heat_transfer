import sys
#sys.path.append('/home/sid/libermate')

import numpy as np
import scipy
#import matcompat
import scipy.sparse
import scipy.linalg

# if available import pylab (from matlibplot)
try:
    import matplotlib.pylab as plt
except ImportError:
    pass

def getSoln(L, S):

    # Local Variables: i, RHS, j, L, n, nx, ny, S, LHS, phiNew, ind
    # Function calls: size, zeros, getSoln
    n = S.shape
    nx = n[0]
    ny = n[1]
    RHS = np.zeros((np.dot(nx, ny)))
    ind = 0.
    for j in np.arange(1., (ny)+1):
        for i in np.arange(1., (nx)+1):
            ind = ind+1.
            RHS[int(ind)-1] = S[int(i)-1,int(j)-1]
            
        
#    RHS = RHS.conj().T
    LHS = np.linalg.solve(L, RHS)
#    LHS = scipy.sparse.linalg.spsolve(L,RHS)
    ind = 0.
    phiNew = np.zeros((nx, ny))
    for j in np.arange(1., (ny)+1):
        for i in np.arange(1., (nx)+1):
            ind = ind+1.
            phiNew[int(i)-1,int(j)-1] = LHS[int(ind)-1]
            
        
    return [phiNew]
