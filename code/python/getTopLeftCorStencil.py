import sys
#sys.path.append('/home/sid/libermate')

import numpy as np
import scipy
#import matcompat

# if available import pylab (from matlibplot)
try:
    import matplotlib.pylab as plt
except ImportError:
    pass

def getTopLeftCorStencil(D, L, M, T, B, h, k, gammayHi, etayHi, syHi, gammaxLo, etaxLo, sxLo):

    # Local Variables: gammaxLo, B, D, sxLo, h, rhs, M, L, etaxLo, gammayHi, lhs, T, etayHi, k, syHi
    # Function calls: getTopLeftCorStencil, zeros
    #% D*u(i,j) -L*u(i-1,j) -M*u(i+1,j) -T*u(i,j+1) -B*u(i,j-1) = S
    #% lhs(1) = ind; lhs(2) = ind-1; lhs(3) = ind+1; lhs(4) = ind+jump; lhs(5) =
    #% ind-jump
    lhs = np.zeros((5))
    if gammayHi == 0.:
        #% dirichilet
        lhs[0] = D
        lhs[1] = -L
        lhs[2] = -M
        lhs[3] = 0.
        lhs[4] = -B
        rhs = np.dot(T, syHi)
    else:
        #% neumann or robin
        lhs[0] = D+(np.dot(np.dot(2.*k, etayHi), T)/gammayHi)
        lhs[1] = -L
        lhs[2] = -M
        lhs[3] = 0.
        lhs[4] = -B-T
        rhs = np.dot(np.dot(2.*k, syHi), T)/gammayHi
        
    
    if gammaxLo == 0.:
        #% dirichilet on left
        rhs = rhs+np.dot(L, sxLo)
    else:
        #% neumann or robin on left
        lhs[0] = lhs[0]-(np.dot(np.dot(2.*h, etaxLo), L)/gammaxLo)
        lhs[2] = lhs[2]-L
        rhs = rhs-(np.dot(np.dot(2.*h, sxLo), L)/gammaxLo)
        
    
    return [lhs, rhs]
