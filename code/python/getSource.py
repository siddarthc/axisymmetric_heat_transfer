import sys
#sys.path.append('/home/sid/libermate')

import numpy as np
import scipy
#import matcompat

from get_rFace import get_rFace

# if available import pylab (from matlibplot)
try:
    import matplotlib.pylab as plt
except ImportError:
    pass

def getSource(rCell, z):

    # Local Variables: q1, z, f, i, j, n, rFace, nz, q2, nr, rCell
    # Function calls: get_rFace, getSource, zeros, size
    n = rCell.shape
    nr = n[0]
    nz = n[1]
    rFace = get_rFace(rCell)
    f = np.zeros((int(nr), int(nz)))
    q1 = 1.712e5
    q2 = 1.712e5
#    q1 = 0.
#    q2 = 0.
    for j in np.arange(1., (nz)+1):
        for i in np.arange(1., (nr)+1):
            if rCell[int(i)-1,int(j)-1]<=1.04:
                f[int(i)-1,int(j)-1] = q1
                #%*rCell(i,j);
            else:
                f[int(i)-1,int(j)-1] = q2
                #%*rCell(i,j);
                
            
            
        
    return [f]
